//1. forEach перебирає кожен елемент масиву і робить з ним якусь дію
//2. Очистити масив можна за допомогою array.length = 0, чи array = []
//3. Чи є змінна масивом можна перевірити за допомогою методу Array.isArray()

let data = ['hello', 'world', 23, '23', null];
function filterBy(array, type) {
    let result = [];
    for(let a of array){
        if(typeof a !== type) {
            result.push(a);
        }
    }
    return result;
}
   
console.log(filterBy(data, 'string'));

